# The Game Pcc

## Description

The project is a Tetris game with scoring.

## Controls

### In game:

W - Rotate tetromino

A - Move left

S - Move down

D - Move right

### On "Game over" screen:
Q - quit

## Testing 
All functionality testing was done manually.

Valgrind was used to check for memory leaks. Except for **initscr()** and **wrefresh()** functions, which are part of the **ncurses** library, no leaks were found in the rest of the program.

<details><summary>About ncurses leaks (from Ncurses FAQ)</summary>

Testing for Memory Leaks

Perhaps you used a tool such as dmalloc or valgrind to check for memory leaks. It will normally report a lot of memory still in use. That is normal.

The ncurses configure script has an option, --disable-leaks, which you can use to continue the analysis. It tells ncurses to free memory if possible. However, most of the in-use memory is “permanent” (normally not freed).

Any implementation of curses must not free the memory associated with a screen, since (even after calling endwin()), it must be available for use in the next call to refresh(). There are also chunks of memory held for performance reasons. That makes it hard to analyze curses applications for memory leaks. To work around this, build a debugging version of the ncurses library which frees those chunks which it can, and provides the exit_curses() function to free the remainder on exit. The ncurses utility and test programs use this feature, e.g., via the ExitProgram() macro.

[Ncurses FAQ on leaks](https://invisible-island.net/ncurses/ncurses.faq.html#config_leaks)
</details>
